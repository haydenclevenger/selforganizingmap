import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.text.*;
import java.util.*;
import java.util.List; // resolves problem with java.awt.List and java.util.List

/**
 * A class that represents a picture.  This class inherits from 
 * SimplePicture and allows the student to add functionality to
 * the Picture class.  
 * 
 * Copyright Georgia Institute of Technology 2004-2005
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture 
{
  ///////////////////// constructors //////////////////////////////////
  
  /**
   * Constructor that takes no arguments 
   */
  public Picture ()
  {
    /* not needed but use it to show students the implicit call to super()
     * child constructors always call a parent constructor 
     */
    super();  
  }
  
  /**
   * Constructor that takes a file name and creates the picture 
   * @param fileName the name of the file to create the picture from
   */
  public Picture(String fileName)
  {
    // let the parent class handle this fileName
    super(fileName);
  }
  
  /**
   * Constructor that takes the width and height
   * @param width the width of the desired picture
   * @param height the height of the desired picture
   */
  public Picture(int width, int height)
  {
    // let the parent class handle this width and height
    super(width,height);
  }
  
  /**
   * Constructor that takes a picture and creates a 
   * copy of that picture
   */
  public Picture(Picture copyPicture)
  {
    // let the parent class do the copy
    super(copyPicture);
  }
  
  /**
   * Constructor that takes a buffered image
   * @param image the buffered image to use
   */
  public Picture(BufferedImage image)
  {
    super(image);
  }
  
  ////////////////////// methods ///////////////////////////////////////
  
  /**
   * Method to return a string with information about this picture.
   * @return a string with information about the picture such as fileName,
   * height and width.
   */
  public String toString()
  {
    String output = "Picture, filename " + getFileName() + 
      " height " + getHeight() 
      + " width " + getWidth();
    return output;
    
  }
  
  
  public static void main(String[] args) 
  {
     String fileName = FileChooser.pickAFile();
     Picture pictObj = new Picture(fileName);
     pictObj.explore();
  }
  
  //method to turn a picture to black and white//
  public void grayScale()
  {
    Pixel [] pixelArray = this.getPixels();
    for (int i = 0; i < pixelArray.length; i++)
    {
      Pixel p0 = pixelArray[i];
      int r = p0.getRed();
      int g = p0.getGreen();
      int b = p0.getBlue();
      int avg = ((r+g+b)/3);
      p0.setRed(avg);
      p0.setGreen(avg);
      p0.setBlue(avg);
    }
  }
  
  //method to lighten a picture//
  public void lighten()
  {
    for (int x = 0; x < getWidth(); x++)
    {
      for (int y = 0; y < getHeight(); y++)
      {
        Pixel pixel = getPixel(x,y);
        Color color = pixel.getColor();
        color = color.brighter();
        pixel.setColor(color);
      }
    }
  }
  
  //method to darken a picture//
   public void darken()
  {
    for (int x = 0; x < getWidth(); x++)
    {
      for (int y = 0; y < getHeight(); y++)
      {
        Pixel pixel = getPixel(x,y);
        Color color = pixel.getColor();
        color = color.darker();
        pixel.setColor(color);
      }
    }
  }
   
   //copy picture method
   public void copyPicture()
   {
     //setting source file to source picture
     Picture sourcePicture = new Picture(FileChooser.pickAFile());
     Pixel sourcePixel = null;
     Pixel targetPixel = null;
     
     //loop through columns
     for (int sourceX = 0, targetX = 0; sourceX < sourcePicture.getWidth(); sourceX++, targetX++)
     {
       //loop through the rows
       for (int sourceY = 0, targetY = 0; sourceY < sourcePicture.getHeight(); sourceY++, targetY++)
       {
         //set target pixel color to source pixel color
         sourcePixel = sourcePicture.getPixel(sourceX,sourceY);
         targetPixel = this.getPixel(targetX,targetY);
         targetPixel.setColor(sourcePixel.getColor());
       }
     }
   }
   
   //method to copy a selected picture to half it's size onto a selected target image//
   public void scaleDown()
   {
     Picture sourcePicture = new Picture(FileChooser.pickAFile());
     Pixel sourcePixel = null;
     Pixel targetPixel = null;
     
     //loop though columns
     for(int sourceX = 0, targetX = 0; sourceX < sourcePicture.getWidth(); sourceX+=2, targetX++)
     {
       //loop through rows
       for(int sourceY = 0, targetY = 0; sourceY < sourcePicture.getHeight(); sourceY+=2, targetY++)
       {
         sourcePixel = sourcePicture.getPixel(sourceX,sourceY);
         targetPixel = this.getPixel(targetX,targetY);
         targetPixel.setColor(sourcePixel.getColor());
       }
     }
   }
   
   //method to scale a selected picture to 2x it's size onto a selected canvas
   public void scaleUp()
   {
     Picture sourcePicture = new Picture(FileChooser.pickAFile());
     Pixel sourcePixel = null;
     Pixel targetPixel = null;
     
     //loop through the columns
     for(double sourceX=0, targetX=0; sourceX < sourcePicture.getWidth(); sourceX=sourceX+0.5, targetX++)
     {
       //loop through the rows
       for(double sourceY=0, targetY=0; sourceY < sourcePicture.getHeight(); sourceY=sourceY+0.5, targetY++)
       {
         sourcePixel = sourcePicture.getPixel((int)sourceX, (int)sourceY);
         targetPixel = this.getPixel((int)targetX, (int)targetY);
         targetPixel.setColor(sourcePixel.getColor());
       }
     }
   }
           
     
   
  
 
} // this } is the end of class Picture, put all new methods before this
 
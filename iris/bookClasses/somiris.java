/**
This is a file to sort and cluster the Fisher Iris Dataset
@author Hayden Clevenger
@date November 22, 2015
*/

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.io.PrintWriter;
import java.util.Random;
import java.util.List;

public class somiris {

	//////////
	//FIELDS//
	//////////

	//the initial map radius and sqrt(number of clusters)
	private static final int RADIUS = 40;

	//time constant value for exponential decay reductions
	private static final double TIME_CONSTANT = 20;

	//the maximum weight change in any iteration
	private static double MAX_CHANGE = 0;

	//number of vector components
	private static final int NUM_VECTOR_COMPONENTS = 4;

	//number of training vectors
	private static final int NUM_TRAINING_VECTORS = 150;

	//number of iterations
	private static final int NUM_ITERATIONS = 100;

	//weight change threshold
	private static final double CONVERGENCE_THRESHOLD = .0001;

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int choice = 0;

		while(choice != 3) {

			System.out.println("Enter:\n 1 to train\n 2 to classify\n 3 to quit");
			choice = scan.nextInt();

			if(choice == 1)
				train(scan);

			else if(choice == 2)
				test(scan);
		}

		System.out.println("Goodbye");
	}

	/////////////////////
	//ACCESSORY METHODS//
	/////////////////////

	////////////////////
	//TRAINING METHODS//
	////////////////////

	/**
	This method is for training the weight matrix
	@param scan is a Scanner to read user input
	*/
	private static void train(Scanner scan) {

		/////////
		//SETUP//
		/////////

		System.out.println("Enter the name of the training file");
		File inputFile = new File(scan.next());
		System.out.println("Enter the name of the weight output file");
		File weightsFile = new File(scan.next());
		System.out.println("Enter the learning rate alpha value");
		double origAlpha = scan.nextDouble();
		double alpha = origAlpha;

		//array to hold all input neurons and their attributes
		double[][] input = getInput(inputFile);
		//NORMALIZE
		input = normalize(input);

		//creating weight matrix
		double[][][] weights = smartNormalizedWeights(input);

		//list to randomly select neurons
		Integer[] randoms = new Integer[NUM_TRAINING_VECTORS];
		for(int i = 0; i < NUM_TRAINING_VECTORS; i++) {
			randoms[i] = i;
		}
		Collections.shuffle(Arrays.asList(randoms));

		/////////
		//TRAIN//
		/////////

		boolean converged = false;
		int iter = 0;
		while(!converged) {
			iter++;
			MAX_CHANGE = 0;
			for(int i = 0; i < randoms.length; i++) {
				//calculate euclidean distance too all output map nodes and choose winner k
				int[] winnerNeuron = getWinner(weights, input, randoms[i]);

				//update k and neighbors
				weights = update(weights, winnerNeuron[0], winnerNeuron[1], iter, alpha, input, randoms[i]);
			}
			//update alpha and test convergence (two methods)
			alpha = reduceAlpha(origAlpha, iter);

			//two different convergence methods
	//		converged = (iter == NUM_ITERATIONS) ? true : false;
			converged = (MAX_CHANGE > CONVERGENCE_THRESHOLD) ? false : true;
		}

		System.out.println("Converged in " + iter + " iterations");

		//printing weights to file
		printWeights(weights, weightsFile);

		//displaying output map as image
		showMap(weights, input);
	}

	/**
	Method to update a neuron and its neighbors
	Neighborhood in this case is a hexagon
	@param weights is the array of weight vectors
	@param x is the row of k
	@param y is the column of k
	@param iter is the current iteration number
	@param alpha is the learning rate
	@param input is the array of input vectors
	@param neuron is the place in the input array of the current input vector
	@return weights is the updated 2D array of neurons
	*/
	private static double[][][] update(double[][][] weights, int x, int y, int iter, double alpha, double[][] input, int neuron) {
		for(int i = 0; i < RADIUS; i++) {
			for(int j = 0; j < RADIUS; j++) {
				//checking if neuron is within neighborhood of k
				if(distance(x, y, i, j) < getNeighborhoodRadius(iter)) {
					for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
						double neighborhoodRadius = getNeighborhoodRadius(iter);
						double weightChange = neighborhoodCoeff(x,y,i,j,neighborhoodRadius) * alpha * (input[neuron][k] - weights[i][j][k] );
						weights[i][j][k] = weights[i][j][k] + weightChange;
						//updating maximum weight change field for use in convergence
						MAX_CHANGE = (MAX_CHANGE > weightChange) ? MAX_CHANGE : weightChange;
					}
				}
			}
		}
		return weights;
	}

	/**
	Method to normalize input vectors
	@param input is a 2D array of input vectors
	@return normalized : a normalized version of input
	*/
	private static double[][] normalize(double[][] input) {

		double[][] normalized = new double[NUM_TRAINING_VECTORS][NUM_VECTOR_COMPONENTS];
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) { 
			double max = getMax(input, i);
			double min = getMin(input, i);
			double diff = max - min;

			for(int j = 0; j < NUM_TRAINING_VECTORS; j++) {
				normalized[j][i] = (input[j][i] - min) / diff;
			}
		}
		return normalized;
	}

	/**
	Method to create the weight matrix
	This evenly distributes weights across the input range
	@param input is the input array
	*/
	private static double[][][] smartWeights(double[][] input) {
		double[][][] weights = new double[RADIUS][RADIUS][NUM_VECTOR_COMPONENTS];
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {

			//finding high and low values for this vector component
			double low = input[0][i];
			double high = input[0][i];
			for(int j = 0; j < NUM_TRAINING_VECTORS; j++) {
				if(input[j][i] < low)
					low = input[j][i];
				if(input[j][i] > high)
					high = input[j][i];
			}

			//calculating optimal distance between weight vectors
			double optDis = (high-low) / (RADIUS*RADIUS);

			//setting weight vals
			for(int j = 0; j < RADIUS; j++) {
				for(int k = 0; k < RADIUS; k++) {
					weights[j][k][i] = low;
					low += optDis;
				}
			}
		}
		return weights;
	}

	/**
	Method to create a simple weight matrix This randomly distributes weights across the input range
	@param input is the input array
	@return weights is the completed 3D weight vector array
	*/
	private static double[][][] randomWeights(double[][] input) {
		double[][][] weights = new double[RADIUS][RADIUS][NUM_VECTOR_COMPONENTS];
		Random r = new Random();

		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
			double low = getMin(input, i);
			double high = getMax(input, i);
			for(int j = 0; j < RADIUS; j++) {
				for(int k = 0; k < RADIUS; k++) {
					weights[j][k][i] = low + ((high-low)*r.nextDouble());
				}
			}
		}
		return weights;
	}

	/**
	Method to create the normalized weight matrix
	This evenly distributes weights across the input range
	@param input is the input array
	*/
	private static double[][][] smartNormalizedWeights(double[][] input) {
		double[][][] weights = new double[RADIUS][RADIUS][NUM_VECTOR_COMPONENTS];
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {

			//finding high and low values for this vector component
			double low = input[0][i];
			double high = input[0][i];
			for(int j = 0; j < NUM_TRAINING_VECTORS; j++) {
				if(input[j][i] < low)
					low = input[j][i];
				if(input[j][i] > high)
					high = input[j][i];
			}

			//calculating optimal distance between weight vectors
			double optDis = (high-low) / (RADIUS*RADIUS);

			//difference between high and low
			double diff = high-low;
			double actual = low;

			//setting weight vals
			for(int j = 0; j < RADIUS; j++) {
				for(int k = 0; k < RADIUS; k++) {
					weights[j][k][i] = (actual - low) / diff;
					actual += optDis;
				}
			}
		}
		return weights;
	}

	/**
	Method to create a normalized weight matrix
	This randomly distributes weights across the input range
	@param input is the input array
	@return weights is the completed 3D weight vector array
	*/
	private static double[][][] randomNormalizedWeights(double[][] input) {
		double[][][] weights = new double[RADIUS][RADIUS][NUM_VECTOR_COMPONENTS];
		Random r = new Random();
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
			for(int j = 0; j < RADIUS; j++) {
				for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
					weights[j][k][i] = r.nextDouble();
				}
			}
		}
		return weights;
	}

	/**
	Method to get the max value of a vector componenent
	@param input is the 2D array of vectors
	@param component is the component we wish to analyze
	@return max is the maximum value
	*/
	private static double getMax(double[][] input, int component) {

		//making max actual value to start
		double max = input[0][component];

		for(int i = 0; i < input.length; i++) {
			max = (input[i][component] > max) ? input[i][component] : max;
		}
		return max;
	}

	/**
	Method to get the min value of a vector componenent
	@param input is the 2D array of vectors
	@param component is the component we wish to analyze
	@return max is the minimum value
	*/
	private static double getMin(double[][] input, int component) {

		//making min actual value to start
		double min = input[0][component];

		for(int i = 0; i < input.length; i++) {
			min = (input[i][component] < min) ? input[i][component] : min;
		}
		return min;
	}


	/**
	Method to determine a selected neurons distance from k
	Used to determine whether it is in the winner neurons neighborhood
	@param iK is the row coordinate of k
	@param jK is the column coordinate of k
	@param i is the row coordinate of the selected neuron
	@param j is the column coordinate of the selected neuron
	@return the Euclidean distance between the nodes
	*/
	private static double distance(int iK, int jK, int i, int j) {
		return ((iK-i)*(iK-i) + (jK-j)*(jK-j));
	}

	/**
	Method to get the radius of neighborhood
	RADIUS is the map size and original neighborhood
	@param iter is the current iteration number
	*/
	private static double getNeighborhoodRadius(int iter) {
		return (RADIUS * (Math.pow(Math.E, (-iter/TIME_CONSTANT))));
	}

	/**
	Method to reduce the alpha learning rate
	@param alpha is the original learning rate
	@param iter is the current iteration number
	@return the new lowered alpha rate
	*/
	private static double reduceAlpha(double origAlpha, int iter) {
		return origAlpha * Math.pow(Math.E, (-iter/TIME_CONSTANT));
	}

	/**
	Method to make neurons closer to k learn more
	Note: euclidean distance remains squared to avoid expensive sqrt computation
	@param iK is the row coordinate of k
	@param jK is the column coordinate of k
	@param i is the row coordinate of the neuron to compare
	@param j is the column coordinate of the neuron to compare
	@param neighborhoodRadius is the current iteration's neighborhood radius
	@return theta is the neighbrhood coefficient from k
	*/
	private static double neighborhoodCoeff(int iK, int jK, int i, int j, double neighborhoodRadius) {
		double distFromK = Math.pow(iK-i, 2) + Math.pow(jK-j, 2);
		double exp = -distFromK / (2*Math.pow(neighborhoodRadius, 2));
		return Math.pow(Math.E, exp);
	}

	/**
	Method to print the weight matrix to file
	@param weights is the weight matrix
	*/
	private static void printWeights(double[][][] weights, File weightsFile) {

		//creating PrintWriter
		PrintWriter weightsWriter = null;
		try {
			weightsWriter = new PrintWriter(weightsFile);
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println("Problem creating PrintWriter for weight output file");
			System.exit(1);
		}

		for(int i = 0; i < RADIUS; i++) {
			for(int j = 0; j < RADIUS; j++) { 
				for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
					weightsWriter.print(weights[i][j][k] + " ");
				}
			}
			weightsWriter.println();
		}
		weightsWriter.flush();
		weightsWriter.close();
	}

	/**
	Method to create an image of weight output map
	@param weights is the weight matrix
	@param input is the input vectors
	*/
	private static void showMap(double[][][] weights, double[][] input) {
		Picture pic = new Picture(RADIUS*10,RADIUS*10);

		for(int i = 0; i < RADIUS; i++) {

			for(int j = 0; j < RADIUS; j++) {

				double redMax = getMax(input, 0);
				double redMin = getMin(input, 0);
				double redPercentage = (weights[i][j][0] - redMin) / (redMax-redMin);
				int red = (int) (redPercentage*255);

				double greenMax = getMax(input, 1);
				double greenMin = getMin(input, 1);
				double greenPercentage = (weights[i][j][1] - greenMin) / (greenMax-greenMin);
				int green = (int) (greenPercentage*255);

				double blueMax = getMax(input, 2);
				double blueMin = getMin(input, 2);
				double bluePercentage = (weights[i][j][2] - blueMin) / (blueMax-blueMin);
				int blue = (int) (bluePercentage*255);

				//for larger images where every node is a pixel
				//pic.getPixel(i,j).setColor(new java.awt.Color(red,green,blue));

				//for maps with a smaller radius
				//don't forget the radius expansion up top
				for(int o = 0; o < 10; o++) {
					for(int k = 0; k < 10; k++) {
						int x = (i * 10) + o;
						int y = (j * 10) + k;
						pic.getPixel(x,y).setColor(new java.awt.Color(red,green,blue));
					}
				}
			}
		}
		pic.show();
		System.out.println("Please enter filename to save picture to");
		Scanner scan = new Scanner(System.in);
		String filename = scan.next();
		pic.write(filename);
	}

	///////////////////
	//TESTING METHODS//
	///////////////////

	/**
	This method is for testing the weight matrix
	@param scan is a Scanner to read user input
	*/
	private static void test(Scanner scan) {

		/////////
		//SETUP//
		/////////

		//choose between manual data entry or data from file
		int choice = 0;
		while(choice != 1 && choice != 2) {
			System.out.println("Enter:\n 1 to manually enter CPU specifications\n 2 to enter a filename containing formatted CPU specifications");

			choice = scan.nextInt();
		}

		//setup including: outputfile, weightsfile, trainingfile and array, printwriter for results
		System.out.println("Please enter output filename");
		File outputFile = new File(scan.next());

		System.out.println("Please enter the trained weights filename");
		File weightsFile = new File(scan.next());
		double[][][] weights = getWeights(weightsFile);

		//setting up cluster lookup array
		System.out.println("Please enter the filename these weights were trained with");
		File clusterFile = new File(scan.next());
		ArrayList<ArrayList<ArrayList<String>>> clusters = getClusters(clusterFile, weights);
		
		PrintWriter resultsWriter = getWriter(outputFile);

		////////////
		//CLASSIFY//
		////////////

		if(choice == 1) {
			//creating cpu vector array
			double[] cpu = getCPU(scan);
			//classifying
			int[] result = classify(cpu, weights);
			//writing results to file
			printResults(cpu, clusters.get(result[0]).get(result[1]), resultsWriter);
		}
		else {
			System.out.println("Please enter testing filename");
			File inputFile = new File(scan.next());
			Scanner inputScanner = getScanner(inputFile);

			//looping over lines and classifying each one
			while(inputScanner.hasNextLine()) {

				//creating cpu vector array
				String line[] = inputScanner.nextLine().split(",");
				double[] cpu = new double[NUM_VECTOR_COMPONENTS];
				for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
					cpu[i] = Double.parseDouble(line[i]);
				}

				//classifying
				int[] result = classify(cpu, weights);
				//writing results to file
				printResults(cpu, clusters.get(result[0]).get(result[1]), resultsWriter);
			}
		}
		resultsWriter.close();
	}

	/**
	Method to classify an input vector using a saved weights file
	@param cpu is the input vector saved in an array
	@param weights is a 3D array of doubles representing the trained weights
	@return result is an array representing the most similar vector
	*/
	private static int[] classify(double[] cpu, double[][][] weights) {

		//initializing sum to a possible value
		double sum = 0;
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
			sum += Math.pow(weights[0][0][i] - cpu[i], 2);
		}
		int savei = 0;
		int savej = 0;

		//loop to consider all weights
		for(int i = 0; i < RADIUS; i++) {

			int tempSum = 0;
			for(int j = 0; j < RADIUS; j++) {

				for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
					tempSum += Math.pow(weights[i][j][k] - cpu[k], 2);
				}

				//checking for minimum sum
				if(tempSum < sum) {
					savei = i;
					savej = j;
					sum = tempSum;
				}
			}
		}
		int[] result = {savei, savej};
		return result;
	}

	/**
	Method to create a 3D double array of weights from a file
	@param weightsFile is the file to construct from
	@return weights is the 3D double arary of weights
	*/
	private static double[][][] getWeights(File weightsFile) {

		Scanner weightScanner = getScanner(weightsFile);
		double[][][] weights = new double[RADIUS][RADIUS][NUM_VECTOR_COMPONENTS];

		for(int i = 0; i < RADIUS; i++) {
			for(int j = 0; j < RADIUS; j++) {
				for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
					try {
						weights[i][j][k] = weightScanner.nextDouble();
					}
					catch (java.util.InputMismatchException e) {
						System.out.println(e);
						System.exit(1);
					}
				}
			}
		}
		return weights;
	}

	/**
	Method to get clusters for result classification
	@param clusterFile is the file to read input from
	@param weights is the array of trained weights
	@return clusters is the ArrayList matrix of Strings
	*/
	private static ArrayList<ArrayList<ArrayList<String>>> getClusters(File clusterFile, double[][][] weights) {
		//array for classifying
		double[][] tempInput = getInput(clusterFile);
		Scanner clusterScanner = getScanner(clusterFile);

		//creating and initializing data structure to save winner neuron info
		ArrayList<ArrayList<ArrayList<String>>> clusters = new ArrayList<ArrayList<ArrayList<String>>>();
		for(int i = 0; i < RADIUS; i++) {
			ArrayList<ArrayList<String>> temp1 = new ArrayList<ArrayList<String>>();
			clusters.add(temp1);
			for(int j = 0; j < RADIUS; j++) {
				ArrayList<String> temp2 = new ArrayList<String>();
				clusters.get(i).add(temp2);
			}
		}

		//saving cluster info
		for(int i = 0; i < NUM_TRAINING_VECTORS; i++) {
			int[] coordinate = getWinner(weights, tempInput, i);
			clusters.get(coordinate[0]).get(coordinate[1]).add(clusterScanner.nextLine());
		}
		return clusters;
	}

	/**
	Method to get user entered information on a CPU
	@param scan is a Scanner to read user input
	@return cpu which is an array to hold all the CPU information
	*/
	private static double[] getCPU(Scanner scan) {
		double[] cpu = new double[NUM_VECTOR_COMPONENTS];
		System.out.println("Enter CPU cycle time");
		cpu[0] = scan.nextDouble();
		System.out.println("Enter CPU minimum main memory");
		cpu[1] = scan.nextDouble();
		System.out.println("Enter CPU maximum main memory");
		cpu[2] = scan.nextDouble();
		System.out.println("Enter CPU cache memory");
		cpu[3] = scan.nextDouble();
		System.out.println("Enter CPU minimum channels");
		cpu[4] = scan.nextDouble();
		System.out.println("Enter CPU maximum channels");
		cpu[5] = scan.nextDouble();
		System.out.println("Enter CPU published relative performance");
		cpu[6] = scan.nextDouble();

		return cpu;
	}

	/**
	Method to write results to specified file
	@param cpu is the input vector
	@param cluster is an ArrayList of Strings that match the cpu input
	@param resultsWriter is the printWriter mapped to the output file
	*/
	private static void printResults(double[] cpu, ArrayList<String> cluster, PrintWriter resultsWriter) {
		resultsWriter.print("Input: ");
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
			resultsWriter.print(cpu[i]);
			if(i != NUM_VECTOR_COMPONENTS-1)
				resultsWriter.print(",");
		}
		resultsWriter.println();
		resultsWriter.println("Best Matches: ");
		for(int i = 0; i < cluster.size(); i++) {
			resultsWriter.println(cluster.get(i));
		}
		resultsWriter.println();
		resultsWriter.flush();
	}

	/////////////////////////////////////////////
	//METHODS USED IN BOTH TRAINING AND TESTING//
	/////////////////////////////////////////////

	/**
	Method to gather input data into an array
	Leaving out iris name
	@param inputFile is the training file that has input
	@return completed array of input
	*/
	private static double[][] getInput(File inputFile) {
		Scanner inputScanner = getScanner(inputFile);
		double[][] input = new double[NUM_TRAINING_VECTORS][NUM_VECTOR_COMPONENTS];

		for(int i = 0; i < NUM_TRAINING_VECTORS; i++) {

			String[] line = inputScanner.nextLine().split(",");

			//reading in training vectors
			for(int j = 0; j < NUM_VECTOR_COMPONENTS; j++) {
				input[i][j] = Double.parseDouble(line[j]);
			}
		}
		return input;
	}

	/**
	Method to get the winner neuron k
	@param weights is the 3D array of weight vectors
	@param input is the 2D array of input vectors
	@param neuron is the input neuron we are considering
	@return the i and j coordinates of the winner neuron in an int[]
	*/
	private static int[] getWinner(double[][][] weights, double[][] input, int neuron) {
		double sum = 0;
		int savei = 0;
		int savej = 0;

		//initializing sum to a possible value
		for(int i = 0; i < NUM_VECTOR_COMPONENTS; i++) {
			sum += Math.pow(weights[0][0][i] - input[neuron][i], 2);
		}

		//loop to consider all weights
		for(int i = 0; i < RADIUS; i++) {

			for(int j = 0; j < RADIUS; j++) {

				double tempSum = 0; 

				for(int k = 0; k < NUM_VECTOR_COMPONENTS; k++) {
					tempSum += Math.pow((weights[i][j][k] - input[neuron][k]), 2);
				}

				//checking for minimum sum
				if(tempSum < sum) {
					savei = i;
					savej = j;
					sum = tempSum;
				}
			}
		}
		int[] winner = {savei, savej};
		return winner;
	}

	/**
	Method to get a scanner from a specified file
	@param inputFile is the file to produce the scanner of
	@return the created scanner
	*/
	private static Scanner getScanner(File inputFile) {
		Scanner scan = null;
		try {
			scan = new Scanner(inputFile);
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println("Getting scanner from file failed");
		}
		return scan;
	}

	/**
	Method to create a PrintWriter for the specified file
	@param outputFile is the file to produce the PrintWriter of
	@return writer which is the PrintWriter
	*/
	private static PrintWriter getWriter(File outputFile) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(outputFile);
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println("Problem creating PrintWriter for weight output file");
			System.exit(1);
		}
		return writer;
	}
}

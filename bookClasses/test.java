import java.util.ArrayList;
import java.awt.Color;
import java.util.Random;

public class test{

	public static void main(String[] args) {

		Random r = new Random();

		double[][][] weights = new double[10][10][7];
		int[][] input = new int[100][7];

		//initialize weights
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				for(int k = 0; k < 7; k++) {
					weights[i][j][k] = r.nextDouble();
				}
			}
		}

		//initialize input
		for(int i = 0; i < 100; i++) {
			for(int j = 0; j < 7; j++) {
				input[i][j] = r.nextInt(10);
			}
		}

		image(weights, input);
	}

	/**
	Make image
	*/
	private static void image(double[][][] weights, int[][] input) {
		Picture pic = new Picture(10,10);
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				pic.getPixel(i,j).setColor(new java.awt.Color(100,20,30));
			}
		}
		pic.show();
	}
}

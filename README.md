# README #


### Kohonen Self Organizing Map ###

This program implements at Kohonen SOM to cluster various datasets.
The famous Fischer Iris dataset and a dataset of CPU specs are included.
The program has the ability to output a 2D representation of the cluster map but struggles with compatibility issues across platforms.

### How do I get set up? ###

Clone repository and navigate to 'code' folder. Change global specs inside som.java to run different datasets and to alter 2D map output. Run with 'java som'.

### Contribution guidelines ###

All contributions and suggestions welcome!

### Who do I talk to? ###

Hayden Clevenger || hayden.clev@gmail.com